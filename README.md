![Program Running With Keyboard](documentation/images/program_running_with_keyboard.jpg?raw=true "Program Running With Keyboard")
# Gameboy MegaMachine Emulator

This is a backlight display emulator for Look Mum No Computer's GAMEBOY MEGAMACHINE project (https://www.youtube.com/channel/UCafxR2HWJRmMfSdyZXvZMTw) for the purpose of testing the appearance of graphics and animations.  Just like the MegaMachine, this emulator listens on Midi Channel 16, and utilizes MIDI note messages on channel 16 to trigger the power to the corresponding gameboy backlight displays.     

## Getting Started


### Prerequisites

A MIDI Device on your Computer and a way of sending MIDI messages to channel 16.  
Processing.org https://processing.org/download/ (If you're not running the binaries)

### Installing
#### Running With Processing.org IDE
Clone the repository

```
git clone --depth=1 https://gitlab.com/lofi8/gameboymegamachineemu/
```

Open Processing, then file->open, navigate to the directory where you cloned the git, and open src/GameboyMegaMachineEmu/GameboyMegaMachineEmu.pde  
Press the Triangle at the upper left hand side of the window to run.   

![Processing Run](documentation/images/running.jpg?raw=true "Processing Run")


## MIDI Gameboy Mapping
![MIDI Gameboy Mapping](documentation/images/gameboy_midi_map.jpg?raw=true "Title")

## Basic Troubleshooting
### Keyboard Connected But Gameboys Not Responding
Make sure your MIDI device is connected before starting the program  
Run from processing and view the console.  It'll output all discovered MIDI devices on startup.  See if yours is showing up

![Program Console](documentation/images/console.jpg?raw=true "Program Console MIDI Devices")

## Built With

* [Processing.org](http://www.processing.org) - framework 
* [Eclipse](https://www.eclipse.org/) - IDE

## Authors

* **Caleb Mucklow** - https://www.lofi8.com https://gitlab.com/lofi8

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


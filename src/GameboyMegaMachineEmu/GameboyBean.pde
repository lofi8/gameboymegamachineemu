class GameboyBean {
  private static final int COLOR_ON = 0xFF2dfefc; //nice pretty backlit teal
  private static final int COLOR_OFF = 0x00000000;
  
  private int xLocation;
  private int yLocation;
  private int width;
  private int height;
  private boolean on;
  private PApplet parent;
  
  public GameboyBean(PApplet parent, int xLocation, int yLocation, int width, int height, boolean on) {
    super();
    this.xLocation = xLocation;
    this.yLocation = yLocation;
    this.width = width;
    this.height = height;
    this.on = on;
    this.parent = parent;

    
  }
  
  public GameboyBean() {

  }
  
  public int getxLocation() {
    return xLocation;
  }
  public void setxLocation(int xLocation) {
    this.xLocation = xLocation;
  }
  public int getyLocation() {
    return yLocation;
  }
  public void setyLocation(int yLocation) {
    this.yLocation = yLocation;
  }
  public int getWidth() {
    return width;
  }
  public void setWidth(int width) {
    this.width = width;
  }
  public int getHeight() {
    return height;
  }
  public void setHeight(int height) {
    this.height = height;
  }
  public boolean isOn() {
    return on;
  }
  public void setOn(boolean on) {
    this.on = on;
  }
  
  public PApplet getParent() {
    return parent;
  }

  public void setParent(PApplet parent) {
    this.parent = parent;
  }

  void display() {
    parent.noStroke();
    parent.fill(on ? COLOR_ON : COLOR_OFF);
    parent.rect(xLocation, yLocation, width, height); 
  }
  
  
}

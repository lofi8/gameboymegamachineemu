class MidiReceiver {
  //Code shamelessly heavily influenced/inspired/stolen from https://stackoverflow.com/questions/6937760/java-getting-input-from-midi-keyboard?noredirect=1&lq=1
  public MidiReceiver() {
    MidiDevice device;
    MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
    for (int i = 0; i < infos.length; i++) {
      try {
        device = MidiSystem.getMidiDevice(infos[i]);
        // does the device have any transmitters?
        // if it does, add it to the device list
        System.out.println(infos[i]);

        // get all transmitters
        List<Transmitter> transmitters = device.getTransmitters();
        // and for each transmitter

        for (int j = 0; j < transmitters.size(); j++) {
          // create a new receiver
          transmitters.get(j).setReceiver(
              // using my own MidiInputReceiver
              new MidiReceiverHandler(device.getDeviceInfo().toString()));
        }

        Transmitter trans = device.getTransmitter();
        trans.setReceiver(new MidiReceiverHandler(device.getDeviceInfo().toString()));

        // open each device
        device.open();
        // if code gets this far without throwing an exception
        // print a success message
        System.out.println(device.getDeviceInfo() + " Was Opened");

      } catch (MidiUnavailableException e) {
      }
    }
  }
}

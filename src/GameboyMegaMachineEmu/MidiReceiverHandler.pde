class MidiReceiverHandler implements Receiver {

  public String name;

  public MidiReceiverHandler(String name) {
    this.name = name;
  }

  public void send(MidiMessage msg, long timeStamp) {
    processMessage(msg.getMessage());
  }

  public void close() {
  }
  
  public void processMessage(byte[] bytes) {
    /*basic midi information gathered from https://www.cs.cmu.edu/~music/cmsip/readings/MIDI%20tutorial%20for%20programmers.html
    Processing doesn't like me putting binary numbers in here, so had to use decimal notation..
    
    Handy conversion table
    240 = 0b11110000
    144 = 0b10010000
    128 = 0b10000000
     15 = 0b00001111
    
    */
    //Am I a note being turned on or off on channel 16?
    if(bytes.length == 3 && ((bytes[0] & 240) == 144 || (bytes[0] & 240) == 128) && (bytes[0] & 15) == 15) { 
      
      int pitch = bytes[1];
      int velocity = bytes[2];
      boolean power = (bytes[0] & 240) == 144; //is midi note being turned on or off
      
      update(pitch, power);
    }
    
  }

}

// Programmed by Caleb Mucklow
// www.lofi8.com
// gitlab.com/lofi8/
// MIT License

  import javax.sound.midi.*;
  import java.util.List;
  import javax.sound.midi.MidiMessage;
  import javax.sound.midi.Receiver;

  
  private static final int GAMEBOY_DISPLAY_WIDTH = 31; //Width of virtual gameboy display panel in pixels
  private static final int GAMEBOY_DISPLAY_HEIGHT = 28; //Height of virtual gameboy display panel in pixels
  private static final int GAMEBOY_DISPLAY_START_X = 47; //Pixel location on X axis of gameboy display starting location on gameboy image file
  private static final int GAMEBOY_DISPLAY_START_Y = 38; //Pixel location on Y axis of gameboy display starting location on gameboy image file
  private static final int GAMEBOY_X_OFFSET = 134; //Width of gameboy image file.  Used when tiling multiple gameboys on screen
  private static final int GAMEBOY_Y_OFFSET = 124; //Height of gameboy image file.  Used when tiling multiple gameboys on screen
  private static final int MATRIX_WIDTH = 8; //Number of Gameboys across X axis
  private static final int MATRIX_HEIGHT = 6; //Number of Gameboys across Y axis
  private static final int GAMEBOY_START_PITCH = 36; //MIDI Pitch Starting Position
  
  private static final String GAMEBOY_OFF_IMAGE = "gameboy_off.png"; //Image of a powered off gameboy

  
  private PImage gameboyOnImage;
  private PImage gameboyOffImage;
  private static GameboyBean[][] gameboyDisplayMatrix;
  private static PApplet parent;
  private MidiReceiver midiReceiver;

    public void settings(){
        size(GAMEBOY_X_OFFSET * MATRIX_WIDTH, (GAMEBOY_Y_OFFSET * MATRIX_HEIGHT) + 25); //On a Mac, Processing 3.5.3 this is sometimes not honored, I got it working by switching back to Processing 3.4.  
                        //See bug report https://github.com/processing/processing/issues/5791
    }
   

    public void setup(){
        fill(120,50,240);
        background( 38, 38, 38);
        gameboyDisplayMatrix = new GameboyBean[MATRIX_WIDTH][MATRIX_HEIGHT];
        midiReceiver = new MidiReceiver();
        parent = this;
        gameboyOffImage = parent.loadImage(GAMEBOY_OFF_IMAGE);
        
        //initialize all the gameboys
        for(int i = 0; i < MATRIX_WIDTH; i++) {
          for(int j = 0; j < MATRIX_HEIGHT; j++) {
            gameboyDisplayMatrix[i][j] = new GameboyBean(this, 
                    GAMEBOY_DISPLAY_START_X + (i * GAMEBOY_X_OFFSET), 
                    GAMEBOY_DISPLAY_START_Y + (j * GAMEBOY_Y_OFFSET), 
                    GAMEBOY_DISPLAY_WIDTH, 
                    GAMEBOY_DISPLAY_HEIGHT, 
                    false);
            parent.image(gameboyDisplayMatrix[i][j].isOn() ? gameboyOnImage : gameboyOffImage, gameboyDisplayMatrix[i][j].getxLocation() - 14, gameboyDisplayMatrix[i][j].getyLocation() - 12);
          }
        }

    }

    public void draw(){
      for(int i = 0; i < MATRIX_WIDTH; i++) {
        for(int j = 0; j < MATRIX_HEIGHT; j++) {            
          gameboyDisplayMatrix[i][j].display();
          
        }
      }
      
      noLoop(); //after initial draw, all subsequent draws are triggered by a midi update in MidiReceiverHandler
    }
    
    public static void update(int gameboyScreen, boolean power) {
      System.out.println("Gameboy: " + gameboyScreen + " " + power);
      GameboyBean gameboy = pitchToGameboy(gameboyScreen);
      
      if(gameboy != null){
        gameboy.setOn(power);
        parent.redraw();
      }

      
    }
    
    public static GameboyBean pitchToGameboy(int pitch) {
      pitch = pitch - GAMEBOY_START_PITCH;
      int row = MATRIX_HEIGHT - (pitch / MATRIX_WIDTH) - 1;
      int column = pitch % MATRIX_WIDTH;
      
      //reject if outside constraints of array size.
      if(row > MATRIX_HEIGHT - 1 
        || row < 0
        || column > MATRIX_WIDTH - 1
        || column < 0){
          return null;
       }
    
      return gameboyDisplayMatrix[column][row];
    }
